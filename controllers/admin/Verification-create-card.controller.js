const {verificationTheRights} = require("../../services/admin/Verification-Rights_Creating-Cards.service");


const verificationCreatingCardAction = async (req, res, next) => {
    try {
        const IsAllowedCreateCard = await verificationTheRights();
        res.status(200).json({
            permissionForCreating: IsAllowedCreateCard
        });
    } catch (error) {
        next(error)
    }
}

module.exports = {verificationCreatingCardAction}