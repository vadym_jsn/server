const {
  creatingEpisode,
} = require("../../services/admin/Creating-Episode.service");
const Chalk = require("chalk");

const createEpisodeAction = async (req, res, next) => {
  try {
    const createdEpisode = await creatingEpisode(req.body);
    res.status(200).json({ info: "created the episode", episode: createdEpisode });
  } catch (error) {
    next(error);
  }
};

module.exports = {
  createEpisodeAction,
};
