const {
  Registration,
  Authentication,
  EnsureToken,
  GettingCredentials
} = require("../../auth/user/auth.service");
const { setTokenCookie } = require("../../auth/helperAuth.service");
const Errors = require("../../CONSTANTS/Errors_const");
const { ErrorGeneration } = require("../../Helper/ErrorGeneration");
const UnitOfTime = require("../../CONSTANTS/UnitTime_const");

const RegistrationAccountAction = async (req, res, next) => {
  try {
    const {AccessToken} = await Registration(req.body, res);
    res.status(200).json({ info: "Email was sent check your inbox", accessToken: AccessToken });
  } catch (error) {
    next(error);
  }
};

const AuthenticationAction = async (req, res, next) => {
  try {
    const res_Authentication = await Authentication(req.body);
    setTokenCookie(
      res,
      res_Authentication.newRefreshToken,
      2,
      UnitOfTime.days
    );
    res.status(200).json({
      accessToken: res_Authentication.accessToken,
    });
  } catch (error) {
    next(error);
  }
};

const EnsureTokenAction = async (req, res, next) => {
  try {
    if(!req.headers["authorization"]){
      throw ErrorGeneration("permission denied", Errors.customError, 401);
    }
    const refreshToken = req.cookies.refreshToken;
    if (refreshToken === undefined) {
      throw ErrorGeneration("expired cookies", Errors.customError, 401);
    }
    const { accessToken, updatedRefreshToken } = await EnsureToken(
      req.headers["authorization"],
      refreshToken
    );
    if (updatedRefreshToken) {
      setTokenCookie(res, updatedRefreshToken, 2, UnitOfTime.days);
    }
    res.status(200).json({
      accessToken: accessToken,
      info: 'access token was updated'
    });
  } catch (error) {
    next(error);
  }
};

const GettingCredentialsAction = async (req, res, next) =>{
  try{
    const accessOfAccount = {
      userId: req.query.userId,
      x_api_key: req.headers['x-api-key']
    }
    const credentials = await GettingCredentials(accessOfAccount);
    setTokenCookie(res, user.refresh_token, 2, UnitOfTime.days);
    res.status(200).json({acessToken: credentials.accessToken, user: credentials.user});
  }catch (error) {
    next(error)
  }
}


module.exports = {
  RegistrationAccountAction,
  AuthenticationAction,
  EnsureTokenAction,
  GettingCredentialsAction,
};
