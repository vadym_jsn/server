const database = require("../../db");
const Chalk = require("chalk");
const moment = require("moment");
const { Location } = require("../../models/location.Model");
const bookshelf = require("../../db/bookShelf");
const {Card} = require("../../models/Card.Model");

const insertLocationData = async (location) => {
  const createdLocation = await new Location({
    name: location.name,
    type: location.type,
    dimension: location.dimension,
  }).save();
  await database("locations")
    .update({
      url: `${process.env.DOM_NAME}/api/location/${createdLocation.id}`,
    })
    .where({ id: createdLocation.id });
  await insert_Resident_location(location.residents, createdLocation.id);
  const locationWithRelations = await getLocationById(createdLocation.id);
  return locationWithRelations;


};

async function insert_Resident_location(ids_residents = [], location_id) {
  const attached = await new Location({id: location_id})
      .residents()
      .attach(ids_residents);
  return attached;
}

async function checkDuplicateLocation(select = "*", fieldSearching) {
  const qb = Location.query();
  const isDuplicated = await qb.select(select).where(fieldSearching);
  return !!isDuplicated.length;
};

async function getLocationById(location_id){
  const card = await Location.where({id: location_id}).fetch({withRelated: ['residents']})
  return card.toJSON();
};

module.exports = {
  insertLocationData,
  insert_Resident_location,
  getLocationById,
  checkDuplicateLocation,
};
