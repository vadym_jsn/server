const database = require("../../db");
const Chalk = require("chalk");
const {Card} = require("../../models/Card.Model");
const Knex = require("../../db/index");

const insertCardData = async (card) => {
    const qb = Card.query();
    const createdCard = await new Card({
        name: card.name,
        status: card.status,
        species: card.species,
        type: card.type,
        gender: card.gender,
        origin: card.origin.location_id || null,
        location: card.location.location_id || null,
    }).save();
    await database("cards")
        .update({url: `${process.env.DOM_NAME}/api/card/${createdCard.id}`})
        .where({id: createdCard.id});
    await insert_card_episodes(card.card_episodes, createdCard.id);
    const cardWithRelations = await getCardById(createdCard.id)
    return cardWithRelations;
};

async function insert_card_episodes(ids_episodes = [], card_id) {
    const attached = await new Card({id: card_id})
        .episodes()
        .attach(ids_episodes);
    return attached;
}

async function checkDuplicateCard(select = "*", fieldSearching) {
    const qb = Card.query();
    const isDuplicated = await qb.select(select).where(fieldSearching);
    return !!isDuplicated.length;
}

async function getCardById(card_id) {
    const card = await Card.where({id: card_id}).fetch({withRelated: ['episodes']})
    return card.toJSON();
}

async function deleteCardById(id) {
    const qb = new Card().query();
    const deleted = await qb.where({id: id}).del();
    return deleted;
}

async function getCountMultiColumn() {
    const qb = Card.query();
    /*The order of execution of functions matters here.*/
    const quantity_cards = await qb.count('id', {as: "quantity_cards"});
    const quantity_sales = await qb.count('number_of_sales', {as: "quantity_sales"}).where('number_of_sales', '>=', '1');
    return {...quantity_sales[0], ...quantity_cards[0]};
}

module.exports = {
    insertCardData,
    checkDuplicateCard,
    getCardById,
    deleteCardById,
    getCountMultiColumn,
};
