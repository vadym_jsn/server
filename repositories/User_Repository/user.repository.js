const database = require("../../db");
const Chalk = require("chalk");
const { User } = require("../../models/User.Model");
const { addTime } = require("../../Helper/functionality_with_time");
const Errors = require("../../CONSTANTS/Errors_const");
const { ErrorGeneration } = require("../../Helper/ErrorGeneration");
const UnitOfTime = require("../../CONSTANTS/UnitTime_const");

const creatingNewUser = async (reqBody, hashPassword, refreshToken) => {
  const user = {
    nick_name: reqBody.nickname,
    password: hashPassword,
    email: reqBody.email,
    refresh_token: refreshToken,
    expires_refreshToken_in: addTime({ amount: 1, unit: UnitOfTime.hours }),
    rating_range: 0,
    cash_balance: 0,
    role: 'user',
    is_email_confirm: !!reqBody.swagger,
    avatar_url: null,
  };
  let { attributes } = await User.forge(user).save(null, { method: "insert" });
  return attributes;
};

const checkingExistUserByEmailAndName = async (nick_name, email) => {
  const qb = new User().query();
  const ResponseQuery = await qb
    .select("id")
    .where("nick_name", nick_name)
    .orWhere({ email: email });
  return !!ResponseQuery.length;
};

const FindByEmail = async (email) => {
  const ResQuery = await database("users").where("email", email);
  if (ResQuery.length) {
    return ResQuery[0];
  } else {
    return false;
  }
};

const UpdateRefreshToken = async (
  NewRefreshToken,
  expires_refreshToken,
  id
) => {
  const updatedRefreshToken = await database.transaction(async (trx) => {
    const isUpdate = await trx("users").where({ id: id }).update({
      refresh_token: NewRefreshToken,
      expires_refreshToken_in: expires_refreshToken,
    });
    if (isUpdate) {
      return NewRefreshToken;
    }
  });
  return updatedRefreshToken;
};
const FindOneByRefreshToken = async (refreshToken, select = "*") => {
  const qb = new User().query();
  const user = await qb.select(select).where({ refresh_token: refreshToken });
  if (user.length) {
    return user[0];
  } else {
    throw ErrorGeneration("user didnt find", Errors.customError, 401);
  }
};

/* parameter SELECT accept array=.select([*columns]) or one string*/
const findUserOneById = async (id, select = "*") => {
  const qb = new User().query();
  const user = await qb.select(select).where({ id: id });
  if (user) {
    return user[0];
  } else {
    throw ErrorGeneration("no have user", Errors.customError, 401);
  }
};

const deleteUserById = async (id) => {
  const qb = new User().query();
  const deleted = await qb.where({id: id}).del();
  return deleted;
};


const testData = async () => {
  await database("locations").insert({ name: "lol" });
};

module.exports = {
  creatingNewUser,
  checkingExistUserByEmailAndName,
  FindByEmail,
  UpdateRefreshToken,
  FindOneByRefreshToken,
  findUserOneById,
  deleteUserById,
};
