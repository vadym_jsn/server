require("dotenv").config({path: `./.env.${process.env.NODE_ENV}`});

const jwt = require("jsonwebtoken");
const userRepository = require("../repositories/User_Repository/user.repository");
const {ErrorGeneration} = require("../Helper/ErrorGeneration");
const Errors = require("../CONSTANTS/Errors_const");

const checkUserRole = (rightUser) => {
    return async (req, res, next) => {
        try {
            const accessToken = req.headers["authorization"].split(" ")[1];
            const decodedInfo = await jwt.verify(accessToken, process.env.ACCESS_TOKEN_SECRET);
            const user = await userRepository.findUserOneById(decodedInfo.id, ['role'])
            if (rightUser === user.role) {
                next();
            } else {
                throw ErrorGeneration("access rights denied", Errors.customError, 403);
            }
        } catch (error) {
            next(error)
        }
    }

}


module.exports = {checkUserRole}