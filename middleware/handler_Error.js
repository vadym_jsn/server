const Errors = require("../CONSTANTS/Errors_const");

function errorHandler(err, req, res, next) {
  switch (true) {
    case err.name === Errors.customError:
      // custom application error
      return res
        .status(err.status || 401)
        .json({ message: err.message, err: true });

    case err.name === "none data":
      return res.status(404).json({ message: err.message, err: true });

    case err.name === Errors.ValidationError:
      return res.status(403).json({ message: err.message, err: true });

    case err.name === "dublicate key":
      return res.status(409).json({ message: err.message });

    case err.name === Errors.EmailError:
      return res.status(403).json({ message: err.message });

    default:
      console.log("error at handlerError default case", err);
      return res.status(500).json({ message: err.message, err: true });
  }
}

module.exports = { errorHandler };
