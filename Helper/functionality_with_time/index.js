const moment = require("moment");

const addTime = ({ amount, unit, needTimeShtamp = false }) => {
  return needTimeShtamp ? moment().add(amount, unit).valueOf() : moment().add(amount, unit).toDate();
};

module.exports = {
  addTime,
};
