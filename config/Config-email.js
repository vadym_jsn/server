require("dotenv").config({ path: `./.env.${process.env.NODE_ENV}` });

const ConfigEmail = {
  email: {
    host: process.env.HOST_MAIL,
    port: process.env.PORT_MAIL,
    secure: process.env.NODE_ENV !== "development",
    auth: {
      user: process.env.USER_MAIL,
      pass: process.env.PASS_MAIL,
    },
  },
};
module.exports = ConfigEmail;
