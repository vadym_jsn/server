const NoneprotectedRouters = [
    "/api/user/registration",
    "/api/user/authentication",
    "/api/user/GettingCredentials",
    "/api/user/ensureToken",
    "/test",
];

module.exports = NoneprotectedRouters;
