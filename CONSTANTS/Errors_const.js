const Errors = {};
Object.defineProperties(Errors, {
  duplicateKey: {
    value: "dublicate key",
    writable: false,
  },
  customError: {
    value: "custom error",
    writable: false,
  },
  ValidationError: {
    value: "Validation Error",
    writable: false,
  },
  noneData: {
    value: "none data",
    writable: false,
  },
  EmailError:{
    value: 'email error',
    writable: false,
  }
});

module.exports = Errors;
