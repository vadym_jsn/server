const CronJob = require("cron").CronJob;
const userRepository = require("../../repositories/User_Repository/user.repository");

class Cron {
  constructor({ time, executeOneTime = false }) {
    this.time = time;
    this.executeOneTime = executeOneTime;
  }
  static executeCron(task) {
    task.start();
  }

   checkConfirmedEmail(userId) {
    const task = new CronJob(this.time, async () => {
        const isConfirmEmail = await userRepository.findUserOneById(userId, ["is_email_confirm"]);
        if(!isConfirmEmail){
            await userRepository.deleteUserById(userId);
        }
      console.log('Cron started')
      if (this.executeOneTime) {
        task.stop();
      }
    });
    Cron.executeCron(task);
  }
}

module.exports = Cron;
