const Joi = require("joi");
const Chalk = require("chalk");
const {REGEX_NOT_ONLY_NUMBER} = require("../CONSTANTS/Regex_const");

const Episode_schema = Joi.object().keys({
    name: Joi.string()
        .allow(null, "")
        .pattern(REGEX_NOT_ONLY_NUMBER)
        .required()
        .error((errors) => {
            errors.forEach((error) => {
                switch (error.code) {
                    case "any.required":
                        error.message = `${error.local.key} of episode should not be a blank`;
                        break;
                    case "string.empty":
                        error.message = `${error.local.key} of episode should not be a blank`;
                        break;
                    case "string.pattern.base":
                        error.message = `${error.local.key} of episode should not contain only number`;
                        break;
                    default:
                        error.message = `Validation Error`;
                        break;
                }
            });
            return errors;
        }),
    air_date: Joi.string()
        .pattern(REGEX_NOT_ONLY_NUMBER)
        .allow(null, "")
        .empty(["", null])
        .error((errors) => {
            errors.forEach((error) => {
                switch (error.code) {
                    case "string.pattern.base":
                        error.message = `${error.local.key} of episode should not contain only number`;
                        break;
                    default:
                        error.message = `Validation Error`;
                        break;
                }
            });
            return errors;
        })
        .default("unknown"),
    episode: Joi.string()
        .pattern(REGEX_NOT_ONLY_NUMBER)
        .allow(null, "")
        .empty(["", null])
        .error((errors) => {
            errors.forEach((error) => {
                switch (error.code) {
                    case "string.pattern.base":
                        error.message = `${error.local.key} of episode should not contain only number`;
                        break;
                    default:
                        error.message = `Validation Error`;
                        break;
                }
            });
            return errors;
        })
        .default("unknown"),
    characters: Joi.array()
        .items(
            Joi.object().keys({
                card_id: Joi.number()
                    .allow("", null)
                    .error((errors) => {
                        errors.forEach((error) => {
                            switch (error.code) {
                                case "number.base":
                                    error.message = `${error.local.key} of episode should contain only number`;
                                    break;
                                default:
                                    error.message = `Validation Error`;
                                    break;
                            }
                        });
                        return errors;
                    }),
            })
        )
        .default([]),
});
module.exports = {Episode_schema};
