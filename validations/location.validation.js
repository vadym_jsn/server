const Joi = require("joi");
const Chalk = require("chalk");
const {REGEX_NOT_ONLY_NUMBER} = require("../CONSTANTS/Regex_const");

const Location_schema = Joi.object().keys({
    name: Joi.string()
        .required()
        .pattern(REGEX_NOT_ONLY_NUMBER)
        .error((errors) => {
            errors.forEach((error) => {
                switch (error.code) {
                    case "any.required":
                        error.message = `${error.local.key} of location should not be a blank`;
                        break;
                    case "string.empty":
                        error.message = `${error.local.key} of location should not be a blank`;
                        break;
                    case "string.pattern.base":
                        error.message = `${error.local.key} of location should not contain only number`;
                        break;
                    default:
                        error.message = `Validation Error`;
                        break;
                }
            });
            return errors;
        }),
    type: Joi.string()
        .pattern(REGEX_NOT_ONLY_NUMBER)
        .allow("", null)
        .empty(["", null])
        .error((errors) => {
            errors.forEach((error) => {
                switch (error.code) {
                    case "string.pattern.base":
                        error.message = `${error.local.key} of location should not contain only number`;
                        break;
                    default:
                        error.message = `Validation Error`;
                        break;
                }
            });
            return errors;
        }).default("unknown"),
    dimension: Joi.string()
        .pattern(REGEX_NOT_ONLY_NUMBER)
        .allow(null, "")
        .empty(["", null])
        .error((errors) => {
            errors.forEach((error) => {
                switch (error.code) {
                    case "string.pattern.base":
                        error.message = `${error.local.key} of location should not contain only number`;
                        break;
                    default:
                        error.message = `Validation Error`;
                        break;
                }
            });
            return errors;
        })
        .default("unknown"),
    residents: Joi.array().items(
        Joi.object().keys({
            card_id: Joi.number()
                .allow("", null)
                .error((errors) => {
                    errors.forEach((error) => {
                        switch (error.code) {
                            case "number.base":
                                error.message = `${error.local.key} of location should contain only number`;
                                break;
                            default:
                                error.message = `Validation Error`;
                                break;
                        }
                    });
                    return errors;
                }),
        })
    ).default([]),
});

module.exports = {Location_schema};
