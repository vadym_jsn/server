const Joi = require("joi");
const Chalk = require("chalk");
const { ErrorGeneration } = require("../Helper/ErrorGeneration");
const Errors = require("../CONSTANTS/Errors_const");
const { REGEX_NOT_ONLY_NUMBER } = require("../CONSTANTS/Regex_const");

const Card_schema = Joi.object().keys({
  name: Joi.string()
    .required()
    .regex(REGEX_NOT_ONLY_NUMBER)
    .error((errors) => {
      errors.forEach((error) => {
        switch (error.code) {
          case error.code === "any.required":
            error.message = `${error.local.key} of card should not be a blank`;
            break;
          case "string.empty":
            error.message = `${error.local.key} of card should not be a blank`;
            break;
          case "string.pattern.base":
            error.message = `${error.local.key} of card should not contain only number`;
            break;

          // eslint-disable-next-line no-fallthrough
          default:
            error.message = `Validation Error`;
            break;
        }
      });
      return ErrorGeneration(errors[0].message, Errors.ValidationError);
    }),
  /* Set the default field if not specified */
  status: Joi.string()
      .regex(REGEX_NOT_ONLY_NUMBER)
      .allow("", null)
      .empty(["", null])
      .error((errors) => {
        errors.forEach((error) => {
          switch (error.code) {
            case "string.pattern.base":
              error.message = `${error.local.key} of card should not contain only number`;
              break;
            default:
              error.message = `Validation Error`;
              break;
          }
        });
        return errors;
      })
      .default("unknown"),
  species: Joi.string()
      .regex(REGEX_NOT_ONLY_NUMBER)
      .allow("", null)
      .empty(["", null])
      .error((errors) => {
        errors.forEach((error) => {
          switch (error.code) {
            case "string.pattern.base":
              error.message = `${error.local.key} of card should not contain only number`;
              break;
            default:
              error.message = `Validation Error`;
              break;
          }
        });
        return errors;
      })
      .default("unknown"),
  type: Joi.string()
      .regex(REGEX_NOT_ONLY_NUMBER)
      .allow(null, "")
      .error((errors) => {
        errors.forEach((error) => {
          switch (error.code) {
            case "string.pattern.base":
              error.message = `${error.local.key} of card should not contain only number`;
              break;
            default:
              error.message = `Validation Error`;
              break;
          }
        });
        return errors;
      }),
  gender: Joi.string()
      .regex(REGEX_NOT_ONLY_NUMBER)
      .allow(null, "")
      .empty(["", null])
      .error((errors) => {
        errors.forEach((error) => {
          switch (error.code) {
            case "string.pattern.base":
              error.message = `${error.local.key} of card should not contain only number`;
              break;
            default:
              error.message = `Validation Error`;
              break;
          }
        });
        return errors;
      })
      .default("unknown"),
  card_episodes: Joi.array().items(
    Joi.object().keys({
      episode_id: Joi.number().allow(null, ""),
    })
  ).default([]),
  origin: Joi.object()
    .keys({
        location_id: Joi.number().allow(null, ""),
    })
    .default({}),
  location: Joi.object()
    .keys({
      location_id: Joi.number().allow(null, ""),
    })
    .default({}),
});


module.exports = { Card_schema };


