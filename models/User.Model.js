const bookshelf = require("../db/bookShelf");

const User = bookshelf.model("User", {
  hasTimestamps: ["created", "updated_at"],
  tableName: "users",
  hidden: ["password"],

  cards_user: function () {
    return this.belongsToMany("Card", "cards_user", "user_id", "card_id");
  },
  rooms: function () {
    return this.belongsToMany("Room", "rooms_users", "user_id", "room_id");
  },
});
module.exports = { User };
