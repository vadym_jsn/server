const bookshelf = require("../db/bookShelf");

// eslint-disable-next-line no-redeclare
const Location = bookshelf.model("Location", {
  hasTimestamps: ['created', 'updated_at'],
  tableName: "locations",

  residents: function () {
    return this.belongsToMany(
      "Card",
      "location_residents",
      " location_id",
      "card_id"
    );
  },
  card_origin: function () {
    return this.hasMany("Card", "origin");
  },
  card_location: function () {
    return this.hasMany("Card", "location");
  },
});

// eslint-disable-next-line no-undef
module.exports = { Location };
