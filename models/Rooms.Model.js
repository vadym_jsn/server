const bookshelf = require("../db/bookShelf");
const Chalk = require("chalk");
require("./User.Model");
require("./Messages.Model");
const Room = bookshelf.model("Room", {
  tableName: "rooms",
  user_id: function () {
    return this.belongsTo("User", "user_id");
  },
  messages: function () {
    return this.belongsToMany(
      "Message",
      "room_messages",
      "room_id",
      "message_id"
    );
  },
  users: function () {
    return this.belongsToMany("User", "rooms_users", "user_id", "room_id");
  },
});

module.exports = { Room };
