const bookshelf = require("../db/bookShelf");
require("./User.Model");
require("./Lot.Model");

const Transactions = bookshelf.model("Transactions", {
  tableName: "transactions",
  user_id: function () {
    return this.belongsTo("User", "user_id");
  },
  lot_id: function () {
    return this.belongsTo("Lot", "lot_id");
  },
});

module.exports = { Set };
