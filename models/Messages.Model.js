const bookshelf = require("../db/bookShelf");
const Chalk = require("chalk");
require("./User.Model");

const Message = bookshelf.model("Message", {
  tableName: "messages",
  user_id: function () {
    return this.belongsTo("User", "user_id");
  },
  room: function () {
    return this.belongsToMany("Room", "room_messages", "room_id", "message_id");
  },
});

module.exports = { Message };
