require("dotenv").config({path: `./.env.${process.env.NODE_ENV}`});

const express = require("express");
const Admin_episode_router = express.Router();
const {createEpisodeAction} = require("../../controllers/admin/Create-Episode.controller");
const {validationMiddleware} = require("../../middleware/validation.middleware");
const {Episode_schema} = require("../../validations/eposode.validation");
const {checkUserRole} = require("../../middleware/checkUserRole");

Admin_episode_router.route("admin/creating/episode").post(
    checkUserRole('admin'),
    validationMiddleware(Episode_schema, "body"),
    createEpisodeAction
);

module.exports = Admin_episode_router;
