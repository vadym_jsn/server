const express = require("express");
const Admin_card_router = express.Router();
const {deleteCardAction} = require("../../controllers/admin/Delete-Card.controller");
const {CreatCardAction} = require("../../controllers/admin/Create-Card.controller");
const {verificationCreatingCardAction} = require("../../controllers/admin/Verification-create-card.controller");
const {validationMiddleware} = require("../../middleware/validation.middleware");
const {checkUserRole} = require("../../middleware/checkUserRole");
const {Card_schema} = require("../../validations/Card.validation");

Admin_card_router.route("/admin/creating/card").post(
    checkUserRole('admin'),
    validationMiddleware(Card_schema, "body"),
    CreatCardAction
);
Admin_card_router.route("/admin/delete/card/:id").delete(
    checkUserRole('admin'),
    deleteCardAction
)
Admin_card_router.route("/admin/permission/creating/card").get(
    checkUserRole('admin'),
    verificationCreatingCardAction
);


module.exports = Admin_card_router
