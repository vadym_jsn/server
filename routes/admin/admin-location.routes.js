require("dotenv").config({path: `./.env.${process.env.NODE_ENV}`});

const express = require("express");
const Admin_location_router = express.Router();
const {validationMiddleware} = require("../../middleware/validation.middleware");
const {Location_schema} = require("../../validations/location.validation");
const {createLocationAction} = require("../../controllers/admin/Create-Location.controller");
const {checkUserRole} = require("../../middleware/checkUserRole");

Admin_location_router.route("/admin/creating/location").post(
    checkUserRole('admin'),
    validationMiddleware(Location_schema, "body"),
    createLocationAction
);

module.exports = Admin_location_router;
