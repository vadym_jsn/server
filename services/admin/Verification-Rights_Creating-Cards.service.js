require("dotenv").config({path: `./.env.${process.env.NODE_ENV}`});
const cardRepository = require("../../repositories/Card_Repository/cards.repository");
const axios = require("axios");

const verificationTheRights = async () => {
        const {data: {info}} = await axios.get(`https://rickandmortyapi.com/api/character`);
        const {quantity_sales, quantity_cards} = await cardRepository.getCountMultiColumn();
        return info.count === quantity_cards ? quantity_sales === quantity_cards : true;
};

module.exports = {verificationTheRights}