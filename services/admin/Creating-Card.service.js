require("dotenv").config({ path: `./.env.${process.env.NODE_ENV}` });
const cardRepository = require("../../repositories/Card_Repository/cards.repository");
const { ErrorGeneration } = require("../../Helper/ErrorGeneration");
const Errors = require("../../CONSTANTS/Errors_const");

// TODO make image upload for card

const creatingCard = async (reqBodyCard) => {
  const dublicatedCard = await cardRepository.checkDuplicateCard("id", {
    name: reqBodyCard.name,
    status: reqBodyCard.status,
    species: reqBodyCard.species,
    type: reqBodyCard.type,
    origin: reqBodyCard.origin.location_id || null,
    location: reqBodyCard.location.location_id || null,
  });
  if (dublicatedCard) {
    throw ErrorGeneration("Card exist", Errors.customError, 200);
  }
  const createdCard = await cardRepository.insertCardData(reqBodyCard);
  return createdCard;
};

module.exports = { creatingCard };
