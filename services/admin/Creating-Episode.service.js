require("dotenv").config({ path: `./.env.${process.env.NODE_ENV}` });
const Chalk = require("chalk");
const moment = require("moment");
const episodeRepository = require("../../repositories/Episodes_Repository/episodes.repository");
const {ErrorGeneration} = require("../../Helper/ErrorGeneration");
const Errors = require("../../CONSTANTS/Errors_const");

const creatingEpisode = async (reqBodyCard) => {
  const dublicateEpisode = await episodeRepository.checkDuplicateEpisode('id', {
    name: reqBodyCard.name
  })
  if(dublicateEpisode) throw ErrorGeneration("episode exist", Errors.customError, 200);
  const createdEpisode = await episodeRepository.insertEpisodeData(reqBodyCard);
  return createdEpisode;
};

module.exports = {
  creatingEpisode,
};
