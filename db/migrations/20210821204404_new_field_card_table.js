
exports.up = function(knex) {
    return knex.schema.table('cards', table => {
        table.integer('number_of_sales').default(0);
    })
};

exports.down = function(knex) {
    return knex.schema.table('cards', table => {
        table.dropColumn('number_of_sales');
    })
};
