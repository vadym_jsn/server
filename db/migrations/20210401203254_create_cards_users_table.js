exports.up = function (knex, Promise) {
  return knex.schema.createTable("cards_user", (table) => {
    table.integer("user_id").unsigned();
    table.integer("card_id").unsigned();

    table.foreign("user_id").references("id").inTable("users").onDelete('CASCADE');
    table.foreign("card_id").references("id").inTable("cards").onDelete('CASCADE');
  });
};

exports.down = function (knex, Promise) {
  return knex.schema.dropTable("cards_user");
};
