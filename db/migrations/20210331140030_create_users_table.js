exports.up = function (knex, Promise) {
  return knex.schema.createTable("users", (table) => {
    table.increments("id").primary();
    table.string("nick_name").notNull();
    table.string("password").notNull();
    table.string("email").notNull();
    table.string("refresh_token");
    table.date("expires_refreshToken_in");
    table.integer("rating_range");
    table.integer("cash_balance");
    table.string("role");
    table.boolean("is_email_confirm");
    table.string("avatar_url");
    table.timestamp("created").defaultTo(knex.fn.now());
    table.timestamp("updated_at").defaultTo(knex.fn.now());
    table.unique(["email", "nick_name"]);
  });
};

exports.down = function (knex, Promise) {
  return knex.schema.dropTable("users");
};
