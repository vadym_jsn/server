exports.up = function (knex, Promise) {
  return knex.schema.createTable("transactions", (table) => {
    table.increments("id").primary();
    table.integer("user_id").unsigned();
    table.integer("lot_id").unsigned();
    table.timestamp("created_at").defaultTo(knex.fn.now());

    table.foreign("user_id").references("id").inTable("users").onDelete('CASCADE');
    table.foreign("lot_id").references("id").inTable("lots").onDelete('CASCADE');
  });
};

exports.down = function (knex, Promise) {
  return knex.schema.dropTable("transactions");
};
