exports.up = function (knex, Promise) {
  return knex.schema.createTable("cards", (table) => {
    table.increments("id").primary();
    table.string("name");
    table.string("status");
    table.string("species");
    table.string("type");
    table.string("gender");
    table.integer("origin").unsigned();
    table.integer("location").unsigned();
    table.string("image");
    table.string("url");
    table.string("created");
    table.string("updated_at");

    table.foreign("origin").references("id").inTable("locations").onDelete('CASCADE');
    table.foreign("location").references("id").inTable("locations").onDelete('CASCADE');
  });
};

exports.down = function (knex, Promise) {
  return knex.schema.dropTable("cards");
};
