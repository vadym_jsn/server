exports.up = function (knex, Promise) {
  return knex.schema.createTable("rooms", (table) => {
    table.increments("id").primary();
    table.string("name_room");
  });
};

exports.down = function (knex, Promise) {
  return knex.schema.dropTable("rooms");
};
