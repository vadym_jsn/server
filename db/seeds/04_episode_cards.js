const axios = require("axios");

const getEpisodes = async () => {
  let totalPages = 3;
  let episodes = [];
  for (let i = 1; i <= totalPages; i++) {
    let {
      data: { results },
    } = await axios.get(`https://rickandmortyapi.com/api/episode?page=${i}`);

    episodes.push(...results);
  }
  return episodes;
};

exports.seed = async function (knex) {
  const episodes = await getEpisodes();
  const episode_cards = [];
  let objEpisodes = {};

  for (let i = 0; i < episodes.length; i++) {
    if (!episodes[i].characters.length) {
      objEpisodes = {
        card_id: null,
        episode_id: episodes[i].id,
      };
      episode_cards.push(objEpisodes);
    }
    for (let j = 0; j < episodes[i].characters.length; j++) {
      const card_id = await knex("cards")
        .where({ url: episodes[i].characters[j] })
        .select("id");
      objEpisodes = {
        card_id: card_id[0].id,
        episode_id: episodes[i].id,
      };
      episode_cards.push(objEpisodes);
    }
  }
  return knex("episode_cards").insert(episode_cards);
};
