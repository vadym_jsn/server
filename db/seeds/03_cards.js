const axios = require("axios");

const getAllCards = async () => {
  const page = 34;
  let cards = [];
  for (let i = 1; i <= page; i++) {
    let {
      data: { results },
    } = await axios.get(`https://rickandmortyapi.com/api/character/?page=${i}`);
    let _cards = results.map((card) => {
      delete card.episode;
      delete card.id;
      return card;
    });
    cards.push(..._cards);
  }
  return cards;
};

exports.seed = async function (knex) {
  let cards = await getAllCards();
  for (let i = 0; i < cards.length; i++) {
    const location_id = await knex("locations")
      .where({ url: cards[i].location.url })
      .select("id");
    const origin_id = await knex("locations")
      .where({ url: cards[i].origin.url })
      .select("id");
    cards[i].location = location_id.length ? location_id[0].id : null;
    cards[i].origin = origin_id.length ? origin_id[0].id : null;
  }
  return knex("cards").insert(cards);
};
