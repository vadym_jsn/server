const axios = require("axios");

const getAllEpisodes = async () => {
  let totalPages = 3;
  let eposodes = [];
  for (let i = 1; i <= totalPages; i++) {
    let {
      data: { results },
    } = await axios.get(`https://rickandmortyapi.com/api/episode?page=${i}`);
    let _episodes = results.map((episode) => {
      delete episode.characters;
      delete episode.id;
      return episode;
    });

    eposodes.push(..._episodes);
  }
  return eposodes;
};

exports.seed = async function (knex) {
  let episodes = await getAllEpisodes();
  return knex("episodes").insert(episodes);
};
